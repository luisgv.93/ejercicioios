//
//  ViewController.swift
//  EjercicioGlobalHits
//
//  Created by Luis ramon Gomez vargas on 25/06/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var table:UITableView!
    var items=[TMoviesDTO]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if pasaron24Hrs(){
            guardarMovies()
        }else{
            let movieDAO = TopMoviesDAO()
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            movieDAO.initcontext(contex: context)
            let movies = movieDAO.getMovies()
            self.items = movies.sorted(by: {$0.rating > $1.rating})
            self.table.reloadData()
            
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 166
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "myCell") as! MovieTableViewCell
        cell.titulo.text = items[indexPath.row].titulo
        cell.fecha.text = items[indexPath.row].fechaLanzamiento
        let url = URL(string: "https://image.tmdb.org/t/p/w500"+items[indexPath.row].pathImg)
        if let data = try? Data(contentsOf: url!) {
            cell.imagen.image = UIImage(data: data)
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vcDetalle : DetalleViewController = storyBoard.instantiateViewController(withIdentifier: "DetalleViewController") as! DetalleViewController
        vcDetalle.movie = items[indexPath.row]
        self.navigationController?.present(vcDetalle, animated: true, completion: nil)
    }
    
    func guardarMovies(){
        Request.doRequest.getTopTenMovies(limit: 10) { response in
            DispatchQueue.main.async {
                let moviesDAO=TopMoviesDAO()
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                moviesDAO.initcontext(contex: context)
                moviesDAO.insertarArrayMovies(listaMovies: response)
                let dateFormater = DateFormatter()
                dateFormater.timeZone = TimeZone.current
                dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let fecha = dateFormater.string(from: Date())
                UserDefaults.standard.set(fecha,forKey:"ultimaDescarga")
                self.items = response
                self.table.reloadData()
            }
        }
    }
    func pasaron24Hrs()->Bool{
        if let fechaGuardada = UserDefaults.standard.object(forKey: "ultimaDescarga") as? String{
            print(fechaGuardada)
            let dateFormater = DateFormatter()
            dateFormater.timeZone = TimeZone.current
            dateFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let fechaAlamacenada = dateFormater.date(from: fechaGuardada)
            let cal = Calendar.current
            let components = cal.dateComponents([.hour], from: fechaAlamacenada!,to: Date())
            let diff = components.hour!
            if diff > 24{
                return true
            }else{
                return false
            }
        }else{
            return true
        }
        
    }

}

