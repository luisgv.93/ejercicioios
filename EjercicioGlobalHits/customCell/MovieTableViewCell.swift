//
//  MovieTableViewCell.swift
//  EjercicioGlobalHits
//
//  Created by Luis ramon Gomez vargas on 28/06/21.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var titulo:UILabel!
    @IBOutlet weak var fecha:UILabel!
    @IBOutlet weak var imagen:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
