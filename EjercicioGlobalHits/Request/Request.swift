//
//  Request.swift
//  EjercicioGlobalHits
//
//  Created by Luis ramon Gomez vargas on 25/06/21.
//

import Foundation

class Request{
    static let  doRequest = Request()
    func getTokenUser(completionHandler:@escaping(_ token:String?)->()){
        let urlToken = URL(string: "https://api.themoviedb.org/3/authentication/token/new?api_key=4e14d90310a0180d492a716d598cdacf")
        let response = URLSession.shared.dataTask(with: urlToken!) { data, response, error in
            if error == nil{
                if let responseJson = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary{
                    if responseJson["success"] as! Bool {
                        completionHandler(responseJson["request_token"] as? String)
                    }else{
                        completionHandler(nil)
                    }
                }else{
                    completionHandler(nil)
                }
            }else{
                completionHandler(nil)
            }
        }
        response.resume()
    }
    func getTopTenMovies(limit:Int,completionHandler:@escaping(_ response:[TMoviesDTO])->()){
        getTokenUser { token in
            if token != nil{
                var aux = 0
                var listaTopMovies=[TMoviesDTO]()
                let urlTopMovies = URL(string: "https://api.themoviedb.org/3/movie/top_rated?api_key=4e14d90310a0180d492a716d598cdacf&language=es&page=1")
                let request = URLSession.shared.dataTask(with: urlTopMovies!){data,response,error in
                    if error == nil{
                        if let responseJson = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary{
                            if let listaMovies = responseJson["results"] as? NSArray {
                                for movie in listaMovies{
                                    aux += 1
                                    let movieJson = movie as! NSDictionary
                                    let objMovie = TMoviesDTO()
                                    objMovie.descripcion = ((movieJson["overview"] as? String) != nil) ? movieJson["overview"] as! String : ""
                                    objMovie.fechaLanzamiento = ((movieJson["release_date"] as? String) != nil) ? movieJson["release_date"] as! String : ""
                                    objMovie.pathImg = ((movieJson["poster_path"] as? String) != nil) ? movieJson["poster_path"] as! String : ""
                                    objMovie.rating = ((movieJson["vote_average"] as? NSNumber) != nil) ? (movieJson["vote_average"] as! NSNumber).decimalValue : 0.0
                                    objMovie.titulo = ((movieJson["title"] as? String) != nil) ? movieJson["title"] as! String : ""
                                    listaTopMovies.append(objMovie)
                                    if aux == limit{
                                        break
                                    }
                                }
                                completionHandler(listaTopMovies)
                            }else{
                            }
                        }else{
                        }
                    }else{
                    }
                }
                request.resume()
            }
            
        }
    }
    
}
