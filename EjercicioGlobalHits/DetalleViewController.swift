//
//  DetalleViewController.swift
//  EjercicioGlobalHits
//
//  Created by Luis ramon Gomez vargas on 28/06/21.
//

import UIKit

class DetalleViewController: UIViewController {
    @IBOutlet weak var imagen:UIImageView!
    @IBOutlet weak var titulo:UILabel!
    @IBOutlet weak var fecha:UILabel!
    @IBOutlet weak var rating:UILabel!
    @IBOutlet weak var detalle:UILabel!
    var movie = TMoviesDTO()
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        // Do any additional setup after loading the view.
    }
    func setData(){
        titulo.text = movie.titulo
        fecha.text = movie.fechaLanzamiento
        rating.text = String(describing:movie.rating)
        detalle.text = movie.descripcion
        let url = URL(string: "https://image.tmdb.org/t/p/w500"+movie.pathImg)
        if let data = try? Data(contentsOf: url!) {
            self.imagen.image = UIImage(data: data)
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
