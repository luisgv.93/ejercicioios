//
//  TMoviesDTO.swift
//  EjercicioGlobalHits
//
//  Created by Luis ramon Gomez vargas on 28/06/21.
//

import Foundation

class TMoviesDTO {
    var titulo:String=""
    var pathImg:String=""
    var fechaLanzamiento:String=""
    var descripcion:String=""
    var rating:Decimal=0.0
}
