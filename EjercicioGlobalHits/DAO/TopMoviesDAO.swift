//
//  TopMoviesDAO.swift
//  EjercicioGlobalHits
//
//  Created by Luis ramon Gomez vargas on 28/06/21.
//

import Foundation
import CoreData
import UIKit
class TopMoviesDAO{
    var context:NSManagedObjectContext?
    func initcontext(contex:NSManagedObjectContext){
        self.context = contex
    }
    func insertarArrayMovies(listaMovies:[TMoviesDTO]){
        self.deleteMovies()
        do{
            for movie in listaMovies{
                let entity = NSEntityDescription.entity(forEntityName: "TOPMOVIES", in: context!)
                let movieCD = NSManagedObject(entity: entity!, insertInto: context!)
                movieCD.setValue(movie.descripcion, forKeyPath: "descripcion")
                movieCD.setValue(movie.fechaLanzamiento, forKeyPath: "fecha")
                movieCD.setValue(movie.pathImg, forKeyPath: "rutaImg")
                movieCD.setValue(movie.rating, forKeyPath: "rating")
                movieCD.setValue(movie.titulo, forKeyPath: "titulo")
                 context!.insert(movieCD)
            }
            try context!.save()
        }catch let error as NSError{
            print("No se pudo guardar el registro. \(error)")
        }
    }
    func deleteMovies(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TOPMOVIES")
        do{
            let movies = try context!.fetch(fetchRequest) as! [TOPMOVIES]
            for movie in movies{
                context!.delete(movie)
            }
            try context!.save()
        }catch{}
    }
    func getMovies()->[TMoviesDTO]{
        var listamovies = [TMoviesDTO]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TOPMOVIES")
        do{
            let moviesBase = try context!.fetch(fetchRequest) as! [TOPMOVIES]
            for movie in moviesBase{
                let objMovie = TMoviesDTO()
                objMovie.descripcion = movie.descripcion!
                objMovie.fechaLanzamiento = movie.fecha!
                objMovie.pathImg = movie.rutaImg!
                objMovie.rating = movie.rating as! Decimal
                objMovie.titulo = movie.titulo!
                listamovies.append(objMovie)
            }
        }catch{
            
        }
        return listamovies
    }
}
